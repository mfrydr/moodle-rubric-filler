# Moodle Rubric Filler

It's annoying to have to click n times for every student to fill a grading rubric.

This is a Firefox extension that provides keyboard shortcuts to fill the 
Moodle "Advanced" Grading Rubric with default values. Fewer clicks, faster grading.

## Installation

Clone or download this repo, then open about:debugging in Firefox and
[add a temporary extension](https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/Your_first_WebExtension#installing)

## Quick start 

`Shift+Alt+2` will set the max grade for each level (if not already set)

See `manifest.json` for the remaining shortcuts.

