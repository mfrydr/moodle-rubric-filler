browser.commands.onCommand.addListener((command) => {
  browser.tabs.executeScript({
    file: "defaultGrade.js",
  }).then(() => {
    if (command === "set-min") {
      setLevel('first', false);
    } else if (command === "set-max") {
      setLevel('last', false);
    } else if (command === "set-min-overwrite") {
      setLevel('first', true);
    } else if (command === "set-max-overwrite") {
      setLevel('last', true);
    }
  }).catch(reportError);
 });


function setLevel(choice, overwrite) {
  const gettingActiveTab = browser.tabs.query({active: true, currentWindow: true});
  gettingActiveTab.then((tabs) => {
    browser.tabs.sendMessage(tabs[0].id, {choice, overwrite});
  });
}

function reportError(error) {
  console.error(`Could not inject content script: ${error}`);
}