"use strict";


console.log("defaultGrade.js loaded");


function setLevels(choice='last', overwrite=false) {
  if (!['last', 'first'].includes(choice)) {
    throw new Error("Invalid choice. Must be 'first' or 'last'");
  }
  const criteria = document.getElementById("advancedgrading-criteria");
  const groups = criteria.querySelectorAll('.criterion .levels tr');

  for (const group of groups) { 
    const levels = group.querySelectorAll('.level');
    // only update a criterion if it is not already checked
    if (overwrite || !isChecked(levels)) {
      for (const level of levels) {
        if (level.className.includes(choice)) {
          level.click()
        }
      }
    }
  }
}

function isChecked(levels) {
  return Array.from(levels).map(l => l.ariaChecked).includes("true");
}

browser.runtime.onMessage.addListener(updateLevels);


function updateLevels(request, sender, sendResponse) {
  setLevels(request.choice, request.overwrite);
}